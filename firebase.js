// Import the functions you need from the SDKs you need
import { getApps, initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCsxrvMsJfBNlR5_3VJAeHhgZLglPIT8_E",
  authDomain: "bntbreg.firebaseapp.com",
  projectId: "bntbreg",
  storageBucket: "bntbreg.appspot.com",
  messagingSenderId: "1007212918146",
  appId: "1:1007212918146:web:3a745fa597de5a03da8b9b",
};

// Initialize Firebase
if (!getApps().length) {
  initializeApp(firebaseConfig);
}
const database = getFirestore();
const auth = getAuth();
const provider = new GoogleAuthProvider();

export { database, auth, provider };
