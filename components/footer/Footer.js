import React from "react";

export default function Footer({ margin_top, margin_bottom }) {
  return (
    <div
      style={{
        textAlign: "center",
        marginTop: margin_top,
        marginBottom: margin_bottom,
        fontSize: "10px",
        fontWeight: "300",
        fontFamily: "Poppins, sans-serif",
      }}
    >
      &#169; {new Date().getFullYear()} (LPPNTB) Lembaga Pengembangan pendidikan
      Nusa Tenggara Barat
    </div>
  );
}
