import s from "./header.module.css";
import Image from "next/image";
import Link from "next/link";

export default function Header() {
  return (
    <Link href="/">
      <a>
        <div className={s.container}>
          <Image
            src="/image/logolpp.svg"
            width={400}
            height={200}
            className={s.image}
          />
          <h1 className={s.h1}>Beasiswa NTB</h1>
        </div>
      </a>
    </Link>
  );
}
