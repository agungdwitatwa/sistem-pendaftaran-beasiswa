import React from "react";
import s from "./appHeader.module.css";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Link from "next/link";
import { useAuth } from "../../Auth";
import { auth } from "../../firebase";

export default function AppHeader() {
  const { currentUser } = useAuth();
  return (
    <div className={s.container}>
      <div className={s.wrapper}>
        <Link href="/">
          <a>
            <div
              className={s.icon}
              onClick={() => {
                auth.signOut();
              }}
            >
              <KeyboardBackspaceIcon
                style={{ marginRight: "10px" }}
                fontSize="large"
              />
              Logout
            </div>
          </a>
        </Link>
        <div className={s.user}>
          {currentUser?.email}
          <AccountCircleIcon style={{ marginLeft: "10px" }} />
        </div>
      </div>
    </div>
  );
}
