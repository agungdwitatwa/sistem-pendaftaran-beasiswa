import React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import s from "./select.module.css";

export default function SelectField({ label, items }) {
  const [select, setSelect] = React.useState("");

  const handleChange = (event) => {
    setSelect(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth size="small">
        <InputLabel id="demo-simple-select-label" className={s.label}>
          {label}
        </InputLabel>
        <Select
          value={select}
          label={label}
          onChange={handleChange}
          className={s.select}
        >
          {items.map((item) => (
            <MenuItem key={item} value={item} className={s.selectItem}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
