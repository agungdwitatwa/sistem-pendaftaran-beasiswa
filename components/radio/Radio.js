import * as React from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import Image from "next/image";

export default function ControlledRadioButtonsGroup() {
  const [value, setValue] = React.useState("");

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <FormControl>
      <FormLabel id="demo-controlled-radio-buttons-group">
        Pilih Negara:
      </FormLabel>
      <RadioGroup
        aria-labelledby="demo-controlled-radio-buttons-group"
        name="controlled-radio-buttons-group"
        value={value}
        onChange={handleChange}
      >
        <FormControlLabel
          value="female"
          control={<Radio />}
          label="Female"
          labelPlacement="bottom"
        />
        <FormControlLabel
          value="Polandia"
          control={<Radio />}
          label={
            <Image
              src="https://flagcdn.com/h120/za.png"
              srcset="https://flagcdn.com/h240/id.png 2x"
              height={"100%"}
              width={120}
              alt="South Africa"
            />
          }
          labelPlacement="bottom"
        />
      </RadioGroup>
    </FormControl>
  );
}
