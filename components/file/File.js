import { React, useState } from "react";
import { Dropzone, FileItem } from "@dropzone-ui/react";
import ImageIcon from "@mui/icons-material/Image";
import s from "./file.module.css";

function File({ buttonLabel, widthFile = "200px", Icon = ImageIcon }) {
  const [files, setFiles] = useState([]);
  const updateFiles = (incommingFiles) => {
    //do something with the files
    setFiles(incommingFiles);
    //even your own upload implementation
  };
  const removeFile = (id) => {
    setFiles(files.filter((x) => x.id !== id));
  };

  return (
    <div>
      <div className={s.file} style={{ width: widthFile }}>
        <div className={s.label}>
          <Icon /> {buttonLabel}
        </div>
        <div className={s.box}>
          <Dropzone
            onChange={updateFiles}
            value={files}
            label="Pilih File ..."
            header={false}
            footer={false}
            minHeight="180px"
          >
            {files.map((file) => (
              <FileItem {...file} onDelete={removeFile} key={file.id} info />
            ))}
          </Dropzone>
        </div>
      </div>
    </div>
  );
}

export default File;
