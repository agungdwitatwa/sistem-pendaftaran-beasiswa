import React from "react";
import Link from "next/link";
import s from "./menu.module.css";
import PersonIcon from "@mui/icons-material/Person";
import SchoolIcon from "@mui/icons-material/School";
import ReceiptIcon from "@mui/icons-material/Receipt";
import ArchiveIcon from "@mui/icons-material/Archive";
import FeedIcon from "@mui/icons-material/Feed";

function Menu({ indexActive }) {
  return (
    <div className={s.container}>
      <Link href="/pengumuman">
        <a>
          <div
            className={
              indexActive == "pengumuman" ? s.menu_item_active : s.menu_item
            }
          >
            <FeedIcon />
            Pengumuman
          </div>
        </a>
      </Link>
      <Link href="/data-diri">
        <a>
          <div
            className={
              indexActive == "dataDiri" ? s.menu_item_active : s.menu_item
            }
          >
            <PersonIcon />
            Data Diri
          </div>
        </a>
      </Link>
      <Link href="/data-pendidikan">
        <a>
          <div
            className={
              indexActive == "dataPendidikan" ? s.menu_item_active : s.menu_item
            }
          >
            <SchoolIcon />
            Data Pendidikan
          </div>
        </a>
      </Link>
      <Link href="/data-rekomendasi">
        <a>
          <div
            className={
              indexActive == "dataRekomendasi"
                ? s.menu_item_active
                : s.menu_item
            }
          >
            <ReceiptIcon />
            Data Rekomendasi
          </div>
        </a>
      </Link>
      <Link href="/berkas-berkas">
        <a>
          <div
            className={
              indexActive == "berkasBerkas" ? s.menu_item_active : s.menu_item
            }
          >
            <ArchiveIcon />
            Berkas - Berkas
          </div>
        </a>
      </Link>
    </div>
  );
}

export default Menu;
