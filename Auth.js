import React, { createContext, useEffect, useState, useContext } from "react";
import Router from "next/router";

import { getAuth } from "firebase/auth";
import Landing from "./pages/index";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [loading, setLoading] = useState(false);
  Router.events.on("routeChangeStart", (url) => {
    setLoading(true);
  });

  Router.events.on("routeChangeComplete", (url) => {
    setLoading(false);
  });

  useEffect(() => {
    const auth = getAuth();
    return auth.onIdTokenChanged(async (user) => {
      if (!user) {
        setCurrentUser(null);
        console.log("no user");
        return;
      }
      setCurrentUser(user);
    });
  }, []);
  if (!currentUser) {
    return <Landing />;
  } else {
    return (
      <AuthContext.Provider value={{ currentUser, loading }}>
        {children}
      </AuthContext.Provider>
    );
  }
};

export const useAuth = () => useContext(AuthContext);
