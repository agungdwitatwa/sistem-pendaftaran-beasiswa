import React from "react";
import AppHeader from "../../components/appHeader/AppHeader";
import Menu from "../../components/menu/Menu";
import s from "./rekomendasi.module.css";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import File from "../../components/file/File";
import { Divider, TextField } from "@mui/material";
import { Button } from "@mui/material";
import InputIcon from "@mui/icons-material/Input";
import Footer from "../../components/footer/Footer";

function dataRekomendasi() {
  return (
    <div className={s.container}>
      <AppHeader />
      <div className={s.wrapper}>
        <div className={s.menu}>
          <Menu indexActive={"dataRekomendasi"} />
        </div>
        <div className={s.content}>
          <div className={s.catatan}>
            Catatan : <br />
            <ol>
              <li>Surat Rekomendasi yang dilampirkan minimal 1</li>
              <li>
                Format surat rekomendasi bebas, menyesuaikan dengan instansi
                pemberi rekomendasi
              </li>
              <li>
                Dalam Surat Rekomendasi harus tertera Identitas Pemberi
                Rekomendasi dan Identitas Penerima Rekomendasi dengan jelas
              </li>
              <li>
                Surat Rekomendasi harus terdapat stempel resmi dari Lembaga
                Pemberi Rekomendasi
              </li>
              <li>File Surat Rekomendasi yang di upload harus berformat PDF</li>
            </ol>
            <span style={{ color: "red" }}>
              * Jika surat rekomendasi yang dilampirkan lebih dari satu, surat
              rekomendasi tersebut digabung menjadi satu file PDF
            </span>
          </div>
          <div className={s.isianForm}>
            <File
              buttonLabel={"Surat Rekomendasi :"}
              widthFile={"250px"}
              Icon={PictureAsPdfIcon}
            />
            <div className={s.form}>
              <TextField
                label="Nama Pemberi Rekomendasi"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Jabatan"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Nama Instunsi"
                variant="outlined"
                fullWidth
                size="small"
              />
              <Button
                variant="contained"
                startIcon={<InputIcon />}
                sx={{
                  width: "140px",
                  textTransform: "capitalize",
                  color: "#fff",
                  backgroundColor: "#0f2b5c",
                  marginBottom: "20px",
                  border: "1px solid #0f2b5c",
                  fontFamily: "Poppins, sans-serif",
                  "&:hover": {
                    backgroundColor: "#fff",
                    color: "#0f2b5c",
                    border: "1px solid #0f2b5c",
                  },
                }}
                disableElevation
                size="small"
              >
                Simpan Data
              </Button>
            </div>
          </div>
          <Divider />
          <Footer margin_bottom={"30px"} margin_top={"30px"} />
        </div>
      </div>
    </div>
  );
}

export default dataRekomendasi;
