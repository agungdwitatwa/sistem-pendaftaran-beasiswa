import React from "react";
import Head from "next/head";
import s from "./landing.module.css";
import Header from "../components/header/Header";
import Image from "next/image";
import Button from "@mui/material/Button";
import Footer from "../components/footer/Footer";
import GoogleIcon from "@mui/icons-material/Google";
import DashboardIcon from "@mui/icons-material/Dashboard";
import { signInWithPopup } from "firebase/auth";
import { auth, provider } from "../firebase";
import { useAuth } from "../Auth";
import Link from "next/link";
import LoadingButton from "@mui/lab/LoadingButton";

export default function Home() {
  const loginWithGoogle = () => {
    signInWithPopup(auth, provider);
  };

  const { currentUser, loading } = useAuth();

  return (
    <div>
      <Head>
        <title>Daftar | Beasiswa NTB</title>
        <meta name="description" content="Sistem Pendaftaran Beasiswa NTB" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={s.main_container}>
        <Header />
        <div className={s.body}>
          <Image src="/image/beasiswa.svg" width={800} height={250} />
        </div>
        <div className={s.content}>
          <p className={s.p}>
            Beasiswa NTB adalah program unggulan Pemerintah Provinsi Nusa
            Tenggara Barat melalui LPPNTB yang bertujuan untuk meningkatkan
            kualitas sumber daya manusia yang ada di daerah Nusa Tenggara Barat.
            Beasiswa NTB merupakan program pengiriman 1000 Cendekia Gemilang,
            dimana putra dan putri Nusa Tenggara Barat yang berprestasi akan
            dikirim dan dibiayai ke luar negeri untuk melanjutkan studinya, baik
            itu untuk jenjang S1, S2 maupun S3.
          </p>
        </div>
        <div className={s.container_button}>
          {currentUser ? (
            loading ? (
              <LoadingButton
                variant="contained"
                startIcon={<DashboardIcon />}
                loading
                loadingPosition="start"
                sx={{
                  textTransform: "none",
                  color: "#fff",
                  backgroundColor: "#0f2b5c",
                  marginBottom: "20px",
                  border: "1px solid #0f2b5c",
                  padding: "10px 20px",
                  fontFamily: "Poppins, sans-serif",
                  "&:hover": {
                    backgroundColor: "#fff",
                    color: "#0f2b5c",
                    border: "1px solid #0f2b5c",
                  },
                }}
                disableElevation
                size="small"
              >
                Form Pengisian Data Diri
              </LoadingButton>
            ) : (
              <Button
                variant="contained"
                startIcon={<DashboardIcon />}
                sx={{
                  textTransform: "none",
                  color: "#fff",
                  backgroundColor: "#0f2b5c",
                  marginBottom: "20px",
                  border: "1px solid #0f2b5c",
                  padding: "10px 20px",
                  fontFamily: "Poppins, sans-serif",
                  "&:hover": {
                    backgroundColor: "#fff",
                    color: "#0f2b5c",
                    border: "1px solid #0f2b5c",
                  },
                }}
                disableElevation
                size="small"
              >
                <Link href="/pengumuman">
                  <a>Form Pengisian Data Diri</a>
                </Link>
              </Button>
            )
          ) : (
            <Button
              variant="contained"
              startIcon={<GoogleIcon />}
              onClick={loginWithGoogle}
              sx={{
                textTransform: "none",
                color: "#fff",
                backgroundColor: "#0f2b5c",
                marginBottom: "20px",
                border: "1px solid #0f2b5c",
                padding: "10px 20px",
                fontFamily: "Poppins, sans-serif",
                "&:hover": {
                  backgroundColor: "#fff",
                  color: "#0f2b5c",
                  border: "1px solid #0f2b5c",
                },
              }}
              disableElevation
              size="small"
            >
              Daftar/Masuk menggunakan akun google
            </Button>
          )}
        </div>
      </main>

      <footer>
        <Footer margin_top={"90px"} margin_bottom={"20px"} />
      </footer>
    </div>
  );
}
