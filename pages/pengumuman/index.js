import React from "react";
import AppHeader from "../../components/appHeader/AppHeader";
import Menu from "../../components/menu/Menu";
import s from "./pengumuman.module.css";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ArchiveIcon from "@mui/icons-material/Archive";
import InterpreterModeIcon from "@mui/icons-material/InterpreterMode";
import FactCheckIcon from "@mui/icons-material/FactCheck";

function Pengumuman() {
  return (
    <div className={s.container}>
      <AppHeader />
      <div className={s.wrapper}>
        <Menu indexActive={"pengumuman"} />
        <div className={s.content}>
          <h3>Pengumuman :</h3>
          <div className={s.catatan}>
            <h4>Perhatian !!</h4>
            <p>
              Pastikan data pada seluruh isian sudah Anda isi dengan data yang
              benar, jangan sampai ada yang terlewatkan. <br /> Kesalahan pada
              saat penginputan data dan pelampiran berkas dapat mempengaruhi
              hasil seleksi. <br /> Silahkan dicek kembali sebelum batas
              pengisian.
              <br />
              <br />
              Batas pengisian data diri bagi calon Awardee Beasiswa NTB adalah{" "}
              <b>Senin, 28 Mei 2022</b> <br />
              Jika melewati tanggal tersebut, data Anda tidak bisa diubah.{" "}
              <br />
              <br />
              Terimakasih,
              <br />
              LPPNTB
            </p>
          </div>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              sx={{ color: "#0f2b5c" }}
            >
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <ArchiveIcon sx={{ mr: "20px" }} /> Hasil Seleksi Berkas
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                {" "}
                Hasil Verifikasi Berkas akan diumumkan pada tanggal 12 Januari
                2022{" "}
              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
              sx={{ color: "#0f2b5c" }}
            >
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <InterpreterModeIcon sx={{ mr: "20px" }} /> Hasil Seleksi
                Wawancara
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>-</Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
              sx={{ color: "#0f2b5c" }}
            >
              <Typography sx={{ display: "flex", alignItems: "center" }}>
                <FactCheckIcon sx={{ mr: "20px" }} /> Hasil Verifikasi Biro
                Kesra
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>-</Typography>
            </AccordionDetails>
          </Accordion>
        </div>
      </div>
    </div>
  );
}

export default Pengumuman;
