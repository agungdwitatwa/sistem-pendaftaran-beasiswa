import "../styles/globals.css";
import { isMobile } from "react-device-detect";
import "../firebase";
import { AuthProvider } from "../Auth";
import Router from "next/router";

function MyApp({ Component, pageProps }) {
  return isMobile ? (
    <div>
      <h2 style={{ textAlign: "center", color: "blue" }}>
        <span style={{ fontSize: "80px" }}>&#9432;</span> <br /> Buka Aplikasi
        Pendaftaran ini dengan PC/LAPTOP untuk tampilan yang lebih baik
      </h2>

      <p style={{ textAlign: "center" }}>
        &#169; {new Date().getFullYear()} (LPPNTB) Lembaga Pengembangan
        pendidikan Nusa Tenggara Barat
      </p>
    </div>
  ) : (
    <AuthProvider>
      <Component {...pageProps} />
    </AuthProvider>
  );
}

export default MyApp;
