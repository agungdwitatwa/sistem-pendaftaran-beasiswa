import { React, useState } from "react";
import AppHeader from "../../components/appHeader/AppHeader";
import File from "../../components/file/File";
import Menu from "../../components/menu/Menu";
import s from "./dataDiri.module.css";
import TextField from "@mui/material/TextField";
import SelectField from "../../components/select/SelectField";
import Button from "@mui/material/Button";
import InputIcon from "@mui/icons-material/Input";
import Footer from "../../components/footer/Footer";
import { Divider } from "@mui/material";
import { useAuth } from "../../Auth";

function dataDiri() {
  return (
    <div className={s.container}>
      <AppHeader />
      <div className={s.wrapper}>
        <div className={s.menu}>
          <Menu indexActive={"dataDiri"} />
        </div>
        <div className={s.content}>
          <div className={s.file}>
            <File buttonLabel={"Upload Foto :"} />
            <File buttonLabel={"Upload KTP :"} />
            <div className={s.note}>
              <h2>Catatan : </h2> <br />
              <ol>
                <li>
                  File yang diupload harus berformat .jpg/.jpeg, dengan ukuran
                  file maksimal 2MB
                </li>
                <li>
                  Pastikan Data yang Anda masukan merupakan data yang valid.
                </li>
                <li>Kevalidan data dapat mempengaruhi proses seleksi</li>
                <li>
                  Pastikan Alamat Email dan Nomor HP / WA masih aktif, karena
                  sewaktu - waktu Admin akan menghubungi melalui Email atau WA
                </li>
              </ol>
            </div>
          </div>
          <div className={s.form}>
            <div className={s.formWrapper}>
              <TextField
                label="Nama Lengkap"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="NIK"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Email"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="No.Hp (WA)"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Tempat, Tanggal Lahir"
                variant="outlined"
                fullWidth
                size="small"
              />
              <SelectField
                label={"Jenis Kelamin"}
                items={["Laki - Laki", "Perempuan"]}
              />
              <SelectField
                label={"Agama"}
                items={[
                  "Islam",
                  "Kristen",
                  "Konghuchu",
                  "Budha",
                  "Hindu",
                  "Katolik",
                ]}
              />
            </div>
            <div className={s.formWrapper}>
              <TextField
                id="outlined-basic"
                label="Nama Lengkap Orang Tua/Wali"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                id="outlined-basic"
                label="Nomor Telepon Orang Tua/Wali yang masih aktif"
                variant="outlined"
                fullWidth
                size="small"
              />
              <SelectField
                label={"Asal Daerah"}
                items={[
                  "Kota Mataram",
                  "Lombok Barat",
                  "Lombok Tengah",
                  "Lombok Utara",
                  "Lombok Timur",
                  "Sumbawa",
                  "Sumbawa Barat",
                  "Bima",
                  "Kota Bima",
                  "Dompu",
                ]}
              />
              <TextField
                id="outlined-basic"
                label="Alamat Domisili Anda Saat Ini"
                variant="outlined"
                fullWidth
                size="small"
              />
            </div>
          </div>
          <div className={s.saveData}>
            <Button
              variant="outlined"
              startIcon={<InputIcon />}
              className={s.button}
              disableElevation
            >
              Simpan Data
            </Button>
          </div>
          <Divider />
          <Footer margin_bottom={"50px"} margin_top={"50px"} />
        </div>
      </div>
    </div>
  );
}

export default dataDiri;
