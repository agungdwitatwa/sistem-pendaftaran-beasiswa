import React from "react";
import s from "./berkas.module.css";
import AppHeader from "../../components/appHeader/AppHeader";
import Menu from "../../components/menu/Menu";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import File from "../../components/file/File";
import { Divider } from "@mui/material";
import { TextField, Button } from "@mui/material";
import SelectField from "../../components/select/SelectField";
import InputIcon from "@mui/icons-material/Input";
import Footer from "../../components/footer/Footer";

function berkasBerkas() {
  return (
    <div className={s.container}>
      <AppHeader />
      <div className={s.wrapper}>
        <Menu indexActive={"berkasBerkas"} />
        <div className={s.content}>
          <div className={s.berkas}>
            <div className={s.berkas_item}>
              <File
                buttonLabel={"Sertifikat Bahasa :"}
                Icon={PictureAsPdfIcon}
              />
              <div className={s.form}>
                <SelectField
                  label={"Jenis Sertifikat Bahasa"}
                  items={["Laki - Laki", "Perempuan"]}
                />
                <TextField
                  label="Skor Bahasa"
                  variant="outlined"
                  size="small"
                  fullWidth
                />
                <div className={s.catatan}>
                  <b style={{ fontSize: "12px" }}>Catatan :</b>
                  <br />
                  <span style={{ fontSize: "12px" }}>
                    File Sertifikat Bahasa yang Anda upload harus berformat PDF
                  </span>
                </div>
              </div>
            </div>
            <div className={s.berkas_item}>
              <File buttonLabel={"Study Plan :"} Icon={PictureAsPdfIcon} />
              <div className={s.catatan}>
                <h5>Catatan</h5>
                <ul>
                  <li>Study Plan yang Anda upload harus berformat .pdf</li>
                  <li>Menggunakan Bahasa Inggris</li>
                  <li>Maksimal 1 Halaman ukuran A4</li>
                  <li>Menggunakan font Times New Roman dengan ukuran 12</li>
                </ul>
              </div>
            </div>
          </div>
          <Divider />
          <div className={s.berkas}>
            <div className={s.berkas_item}>
              <File
                buttonLabel={"Motivation Letter :"}
                Icon={PictureAsPdfIcon}
              />
              <div className={s.catatan}>
                <h5>Catatan</h5>
                <ul>
                  <li>
                    Motivation Letter yang Anda upload harus berformat .pdf
                  </li>
                  <li>Menggunakan Bahasa Inggris</li>
                  <li>Maksimal 1 Halaman ukuran A4</li>
                  <li>Menggunakan font Times New Roman dengan ukuran 12</li>
                </ul>
              </div>
            </div>
            <div className={s.berkas_item}>
              <File
                buttonLabel={"Curriculum Vitae :"}
                Icon={PictureAsPdfIcon}
              />
              <div className={s.catatan}>
                <h5>Catatan</h5>
                <ul>
                  <li>
                    Curriculum Vitae (CV) yang Anda upload harus berformat .pdf
                  </li>
                  <li>Menggunakan Bahasa Inggris</li>
                  <li>Maksimal 1 Halaman ukuran A4</li>
                  <li>Menggunakan font Times New Roman dengan ukuran 12</li>
                </ul>
              </div>
            </div>
          </div>
          <Divider />
          <div className={s.berkas}>
            <div className={s.berkas_item}>
              <File
                buttonLabel={"Paspor (Jika Ada) :"}
                Icon={PictureAsPdfIcon}
              />
              <div className={s.catatan}>
                <h5>Catatan</h5>
                <ul>
                  <li>Paspor yang dilampirkan hanya halaman depan</li>
                  <li>Berformat .pdf</li>
                  <li>Pastikan data Anda terlihat dengan jelas</li>
                </ul>
              </div>
            </div>
            <div className={s.berkas_item}>
              <File
                buttonLabel={"Keterangan Sehat :"}
                Icon={PictureAsPdfIcon}
              />
              <div className={s.catatan}>
                <h5>Catatan</h5>
                <ul>
                  <li>
                    Surat Keterangan dari rumah sakit atau puskesmas terdekat
                  </li>
                  <li>Berformat .pdf</li>
                  <li>Pastikan Identitas Anda tertera dengan jelas</li>
                </ul>
              </div>
            </div>
          </div>
          <Button
            variant="contained"
            startIcon={<InputIcon />}
            sx={{
              width: "140px",
              textTransform: "capitalize",
              color: "#fff",
              backgroundColor: "#0f2b5c",
              marginBottom: "20px",
              border: "1px solid #0f2b5c",
              fontFamily: "Poppins, sans-serif",
              "&:hover": {
                backgroundColor: "#fff",
                color: "#0f2b5c",
                border: "1px solid #0f2b5c",
              },
            }}
            disableElevation
            size="small"
          >
            Simpan Data
          </Button>
          <Divider />
          <Footer margin_top={"50px"} margin_bottom={"20px"} />
        </div>
      </div>
    </div>
  );
}

export default berkasBerkas;
