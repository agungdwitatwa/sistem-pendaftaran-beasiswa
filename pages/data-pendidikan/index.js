import React from "react";
import AppHeader from "../../components/appHeader/AppHeader";
import Menu from "../../components/menu/Menu";
import s from "./dataPendidikan.module.css";
import TextField from "@mui/material/TextField";
import File from "../../components/file/File";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import Footer from "../../components/footer/Footer";
import SelectField from "../../components/select/SelectField";
import Button from "@mui/material/Button";
import InputIcon from "@mui/icons-material/Input";
import { Divider } from "@mui/material";

function dataPendidikan() {
  return (
    <div className={s.container}>
      <AppHeader />
      <div className={s.wrapper}>
        <div className={s.menu}>
          <Menu indexActive={"dataPendidikan"} />
        </div>
        <div className={s.content}>
          <h2>Data Pendidikan Selama Sarjana (S1)</h2>
          <div className={s.wrapper_content}>
            <div className={s.form}>
              <TextField
                label="Nama Kampus Asal"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Program Studi"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="IPK Studi"
                variant="outlined"
                fullWidth
                size="small"
              />
              <TextField
                label="Alamat Kampus"
                variant="outlined"
                fullWidth
                size="small"
              />
            </div>
            <File
              buttonLabel={"Hasil Scan Ijazah :"}
              widthFile={"250px"}
              Icon={PictureAsPdfIcon}
            />
            <File
              buttonLabel={"Hasil Scan Transkrip Nilai :"}
              widthFile={"250px"}
              Icon={PictureAsPdfIcon}
            />
          </div>
          <div className={s.validasi_ijazah}>
            {/* <File buttonLabel={"Hasil Validasi Ijazah :"} widthFile={"250px"} /> */}
            <div className={s.catatan}>
              Catatan : <br />
              <ol>
                <li>Pastikan Ijazah yang Anda upload terlihat jelas</li>
                <li>
                  Pastikan Ijazah yang Anda upload sudah terverifikasi oleh
                  Kemenristekdikti. <br /> Cara cek validitas ijazah :
                </li>
                <ul>
                  <li>
                    Buka link{" "}
                    <a
                      href="https://ijazah.kemdikbud.go.id"
                      target="_blank"
                      style={{ color: "#2c73ed" }}
                      rel="noreferrer"
                    >
                      https://ijazah.kemdikbud.go.id/
                    </a>
                  </li>
                  <li>Isi data pada formulir verifikasi</li>
                  <li>
                    Lihat hasil verifikasi, jika data Anda muncul, berarti
                    ijazah anda sudah terverifikasi. Jika tidak, silahkan
                    hubungi Admin kampus masing-masing
                  </li>
                </ul>
              </ol>
            </div>
          </div>
          <h2>Data pendidikan yang akan ditempuh selama S2</h2>
          <div className={s.wrapper_s2}>
            <SelectField
              label={"Pilih Negara Tujuan"}
              items={["Malaysia", "Polandia", "China", "Taiwan", "Ceko"]}
            />
            <SelectField
              label={"Kampus/Universitas yang dipilih"}
              items={["Malaysia", "Polandia", "China", "Taiwan", "Ceko"]}
            />
            <SelectField
              label={"Program Studi yang dipilih"}
              items={["Malaysia", "Polandia", "China", "Taiwan", "Ceko"]}
            />
          </div>
          <div className={s.saveData}>
            <Button
              variant="outlined"
              startIcon={<InputIcon />}
              className={s.button}
              disableElevation
            >
              Simpan Data
            </Button>
          </div>
          <Divider />
          <Footer margin_top={"50px"} margin_bottom={"50px"} />
        </div>
      </div>
    </div>
  );
}

export default dataPendidikan;
